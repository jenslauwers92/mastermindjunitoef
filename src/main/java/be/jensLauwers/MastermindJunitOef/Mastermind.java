package be.jensLauwers.MastermindJunitOef;

import java.util.Random;


public class Mastermind {
    //final static String INVALID_GUESS_STRUCTURE=null;
    final static String FULL_VALID_GUESS="+";
    final static String SEMI_VALID_GUESS ="-";
    final static String CORRECT_GUESS="++++";
    StringTool st =new StringTool();
    private int guesses =0;
    private String answer=null;
    private boolean finishedGame;

    public Mastermind() {

    }
    public void reset(){
        this.guesses=0;
        this.finishedGame=false;
        //setAnswer(INVALID_GUESS_STRUCTURE);
        createRandomAnswer();
    }
    public int getGuesses(){
        return guesses;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        if(!answer.matches("[1-6]{4}")){
            throw new IllegalArgumentException(("numbers must be from 1 until 6"));
        }
        this.answer=answer;
    }
    public boolean isFinishedGame(){
        return finishedGame;
    }
    protected String createRandomAnswer() {
        // start up
        Random rand = new Random();
        StringBuilder ans = new StringBuilder("123456");
        // shuffle 10 times
        for (int i = 0; i < 10; i++) {
            int pos1, pos2 ;
            do {
                pos1 = rand.nextInt(ans.length());
                pos2 = rand.nextInt(ans.length());
            } while (pos1 == pos2);
            //System.out.printf("pos1: %d   pos2: %d", pos1, pos2);
            char temp = ans.charAt(pos1);
            ans.setCharAt(pos1, ans.charAt(pos2));
            ans.setCharAt(pos2, temp);
            //System.out.println(answer);
        }
        //# resize output
        return ans.substring(2);
    }
    /**
     * This wil compare a Mastermind guess to the answer.
     * the result wil be a combination of + and -.
     * + represents a correct number guessed in the correct position.
     * - represents a correct number guessed, but in the wrong position.
     *
     * @param guess  the guess of the user
     * @param answer the right answer to the game
     * @return result of the guess
     */
    protected String checkGuess(String guess, String answer) {
        StringBuilder resultPos = new StringBuilder();
        StringBuilder resultNeg = new StringBuilder();
        for (int answerIndex = 0; answerIndex < answer.length(); answerIndex++) {
            //System.out.println("answer = " + answer);
            char loopAnswer = answer.charAt(answerIndex);
            //check answer and guess on the same position
            if (loopAnswer == guess.charAt(answerIndex)) {
                resultPos.append(FULL_VALID_GUESS);
                answer = st.setX(answer, answerIndex);
                continue;
            }
            //check answer and guess on all positions
            for (int guessIndex = 0; guessIndex < guess.length(); guessIndex++) {
                if (guessIndex == answerIndex) continue; // skip this one, already checked
                char loopGuess = guess.charAt(guessIndex);
                if (loopGuess == loopAnswer) {
                    answer = st.setX(answer, answerIndex);
                    resultNeg.append(SEMI_VALID_GUESS);
                    break; // break inner for loop
                }
            }
        }
        return resultPos.toString() + resultNeg;
    }

}
