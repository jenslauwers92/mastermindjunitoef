package be.jensLauwers.MastermindJunitOef;



import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    Mastermind app = new Mastermind();


    public static void main(String[] args) {
        App mm = new App();
        mm.startGame();

    }
    public void startGame() {
        // intro
        System.out.println("Welcome to Mastermind!");
        System.out.println("I'm thinking of a 4 digit code, with numbers starting from 1 up to 6.");
        System.out.println("Duplicate values are not allowed.");
        System.out.println(" '+' represents a correct number guessed in the correct position");
        System.out.println(" '-' represents a correct number guessed, but in the wrong position.");
        // startup
        App mm = new App();
        mm.runGame();
    }
     void runGame(){

        String answer = app.createRandomAnswer();
        String goodResult = Mastermind.CORRECT_GUESS;
        String result ="";
        int roundCounter = app.getGuesses();

         do {
             roundCounter++;
             int guess= Guess();
             if(guess !=-1 ){
                 result = app.checkGuess(String.valueOf(guess), answer);
                 if (!result.equals(goodResult)) {
                     System.out.println(result);
                 }
             }
         } while (!result.equals(goodResult));
        System.out.println("You solved it in: " +roundCounter+" guesses!");
         System.out.println("You want another game?(y/n)");
         String yesorno = new Scanner(System.in).nextLine();
         if(yesorno.equals("y")) {
             app.reset();
             runGame();
         }

    }
    public static int Guess() {
        System.out.println("input your guess ");
        String str = new Scanner(System.in).nextLine();
        try {
            if(str.length() == 4 ){
                for (int i =0; i < str.length();i++){
                    for(int j = 1; j < str.length(); j++){
                        if( i!=j && str.charAt(i) == str.charAt(j)|| str.charAt(i)>'6' || str.charAt(i)<'1'){
                            System.out.println("No duplicates allowed and/or numbers must be in range of 1 to 6");
                            return -1;
                        }
                    }
                }
                return Integer.parseInt(str);
            }
            else {
                System.out.println("not a 4 digit code");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("your guess isn't correctly formed");
            return -1;
        }
    }
}
