package be.jensLauwers.MastermindJunitOef;



public class StringTool {
    protected String setX(String str, int xPosition) {
        if (str != null && xPosition < str.length()) {
            if (xPosition == 0)
                str = "x" + str.substring(xPosition + 1);
            else
                str = str.substring(0, xPosition) + "x" + str.substring(xPosition + 1);
        }
        return str;
    }

}
