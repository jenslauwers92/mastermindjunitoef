package be.jensLauwers.MastermindJunitOef;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class AppTest {
    @Test
    void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    @Test
    @DisplayName("Testing inputGuess")
    void testGuess(){

        String[] goodInput ={"1234","2345","1346"};
        String[] badInput ={"123","23457","2222","abcd"};
        for(String w :goodInput){
            InputStream is = new ByteArrayInputStream(w.getBytes());
            System.setIn(is);
            Assertions.assertEquals(Integer.parseInt(w), App.Guess());
        }
        for(String x :badInput){
            InputStream is = new ByteArrayInputStream(x.getBytes());
            System.setIn(is);
            Assertions.assertEquals(-1, App.Guess());
        }

    }
}
