package be.jensLauwers.MastermindJunitOef;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;




class MastermindTest {
    @DisplayName("Test Set and Get answer")
    @Test
    public final void testSetandGetAnswer(){
        Mastermind mastermind=new Mastermind();
        mastermind.setAnswer("1234");
        Assertions.assertEquals("1234", mastermind.getAnswer());
    }
    @DisplayName("Test get guesses")
    @Test
    public final void testGetGuesses(){
        Mastermind mastermind=new Mastermind();

        Assertions.assertEquals(0, mastermind.getGuesses());
    }
    @DisplayName("Test isfinishedGameFunction")
    @Test
    public final void testIsFinishedGame(){
        Mastermind mastermind=new Mastermind();

        Assertions.assertFalse(mastermind.isFinishedGame());

    }
    @DisplayName("Test reset function")
    @Test
    public final void testReset(){
        Mastermind mastermind=new Mastermind();
        mastermind.reset();
        Assertions.assertEquals(0,mastermind.getGuesses() );
        Assertions.assertFalse(mastermind.isFinishedGame());

    }
    @DisplayName("Test CreateRandomAnswer")
    @Test
    public final void testCreateRandomAnswer(){
        Mastermind mastermind=new Mastermind();
        String ans=mastermind.createRandomAnswer();

        Assertions.assertEquals(4,ans.length());


    }
    @DisplayName("Test CheckGuess")
    @Test
    public final void testCheckGuess(){
        Mastermind mastermind=new Mastermind();
        String answer="1234";
        String guess="1256";
        Assertions.assertEquals((Mastermind.FULL_VALID_GUESS)+(Mastermind.FULL_VALID_GUESS),mastermind.checkGuess(guess,answer) );
        String guess2="2356";
        Assertions.assertEquals(Mastermind.SEMI_VALID_GUESS+Mastermind.SEMI_VALID_GUESS,mastermind.checkGuess(guess2,answer) );
        String guess3="1562";
        Assertions.assertEquals(Mastermind.FULL_VALID_GUESS+Mastermind.SEMI_VALID_GUESS,mastermind.checkGuess(guess3,answer) );
        String guess4="1234";
        Assertions.assertEquals(Mastermind.CORRECT_GUESS,mastermind.checkGuess(guess4,answer) );

    }
    @Test
    @DisplayName("throw exception test invalid answer")
    void testException(){
        Mastermind mastermind=new Mastermind();

        Assertions.assertThrows(IllegalArgumentException.class, () -> mastermind.setAnswer("adr"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> mastermind.setAnswer("7895"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> mastermind.setAnswer(""));
    }

}