package be.jensLauwers.MastermindJunitOef;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;



class StringToolTest {
    @DisplayName("Test Set StringX")
    @Test
    public final void testSetX(){
        StringTool st=new StringTool();
        Assertions.assertEquals("xello", st.setX("hello",0));
        Assertions.assertEquals("hxllo", st.setX("hello",1));
        Assertions.assertEquals("hexlo", st.setX("hello",2));
    }

}